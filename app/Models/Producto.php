<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    public $timestamps=false;

    protected $table = 'productos';

    protected $fillable = ['descripcion','precio','stock','estado','imagen'];

    public function boletas(){
        return $this->belongsToMany(Boleta::class,'boleta_producto')->withPivot('cantidad');
    }
}
