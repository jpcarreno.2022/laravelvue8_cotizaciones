<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Materia;

class Alumno extends Model
{
    use HasFactory;
    protected $fillable = ['nombre'];
    public function materias(){
        return $this->belongsToMany(Materia::class,'alumno_materia')->withPivot('cantidad');
    }
}
