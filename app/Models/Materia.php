<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Alumno;

class Materia extends Model
{
    use HasFactory;
    protected $fillable = ['nombre'];
    public function alumnos(){
        return $this->belongsToMany(Alumno::class,'alumno_materia');
    }
}
