<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlumnoMateria extends Model
{
    use HasFactory;
    protected $table = 'alumno_materia';
    protected $fillable = ['alumno_id','materia_id','cantidad'];
}
