<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cliente;
use App\Models\Producto;

class Boleta extends Model
{
    use HasFactory;

    // protected $dates = ['fecha'];
    protected $table = 'boletas';

    protected $fillable = ['fecha','total','cliente_id'];

    public function clientes(){
        return $this->belongsTo(Cliente::class,'id');
    }

    public function productos(){
        return $this->belongsToMany(Producto::class,'boleta_producto')->withPivot('cantidad');
    }
}
