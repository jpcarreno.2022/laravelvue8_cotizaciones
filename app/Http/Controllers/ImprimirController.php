<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Barryvdh\DomPDF\Facade\PDF;
use App\Models\Boleta;
use App\Models\Cliente;
use App\Models\Producto;

class ImprimirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    $today = "1982";
    $pdf = PDF::loadView('sistema_cotizaciones.cotizaciones.imprimir_cotizacion', compact('today'));
    // return $pdf->download('imprimir_cotizacion.pdf', array("Attachment" => false));
    return $pdf->stream('imprimir_cotizacion.pdf', array("Attachment" => false));
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdf($id)
    {
        // $today = "1982";
        // $pdf = PDF::loadView('sistema_cotizaciones.cotizaciones.imprimir_cotizacion', compact('today'));
        // return $pdf->download('imprimir_cotizacion.pdf', array("Attachment" => false));
        // return $pdf->stream('imprimir_cotizacion.pdf', array("Attachment" => false));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->idCotizacion;
        $cotizacion = Boleta::find($id);
        // $productos = $boleta->productos;
        // return $cotizacion;
        $pdf = PDF::loadView('sistema_cotizaciones.cotizaciones.imprimir_cotizacion', compact('cotizacion'));
        // return $pdf->download('imprimir_cotizacion.pdf', array("Attachment" => false));
        return $pdf->stream('imprimir_cotizacion.pdf', array("Attachment" => false));

    //    return $request;
    // return $request->idCotizacion;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
