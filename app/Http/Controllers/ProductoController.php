<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::all();
        return view('sistema_cotizaciones.productos.index',compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sistema_cotizaciones.productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nombre_producto = $request->file('archivo')->getClientOriginalName();
        // Producto::create($request->all());
        // $extension = $request->file('avatar')->extension();
        Producto::create(['descripcion'=>$request->descripcion,'precio'=>$request->precio,'stock'=>$request->stock,'estado'=>$request->estado,'imagen'=>$nombre_producto]);
        $request->file('archivo')->storeAs('public',$nombre_producto);       
        // $path = $request->file('avatar')->storeAs('avatars', $request->user()->id);
        
        // $request->file('archivo')->store('public',$nombre_producto);
        // $request->file('archivo')->store('public');
        // $request->file('archivo')->store('public/otracarpeta');
        return redirect()->route('productos.index');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::find($id);
        // return $producto;
        return view('sistema_cotizaciones.productos.edit',compact('producto'));
        // return view('sistema_cotizaciones.productos.edit',compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Producto::find($id)->update($request->all());
        return redirect()->route('productos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $producto = Producto::find($id);

       if($producto->estado == 0){
        $status = 1;
       }
       elseif($producto->estado == 1){
        $status = 0;
       }

       $producto->update(['estado' => $status]);
       return redirect()->route('productos.index');

    }
}
