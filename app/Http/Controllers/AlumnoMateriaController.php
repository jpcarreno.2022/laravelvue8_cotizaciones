<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alumno;
use App\Models\Materia;
use App\Models\AlumnoMateria;

class AlumnoMateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $alumno = Alumno::find(1);
        $materia = Materia::find(2);
        $alumnos = Alumno::all();
        $materias = Materia::all();
        return view('alumno_materia',compact('alumno','materia','alumnos','materias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //{"nombre_alumno":"2","nombre_materia":"2","cantidad":"45"}
        // return $request->cantidad;
        // $alumno_materia = AlumnoMateria::create(['alumno_id'=>$request->nombre_alumno,'materia_id'=>$request,'cantidad'=>$request->cantidad]);

        // AlumnoMateria::create($request->all());
        // return redirect()->route('alumno_materia.index');

        $alumno = Alumno::find($request->nombre_alumno);
        $materia = Materia::find($request->nombre_materia);
        $alumno->materias()->attach($materia->id,['cantidad'=>$request->cantidad]);
        return redirect()->route('alumno_materia.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
