<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Barryvdh\DomPDF\Facade\PDF;

class ImprimirPdfController extends Controller
{
    public function mostrarPdf($id){
        $today = $id;
        $pdf = PDF::loadView('sistema_cotizaciones.cotizaciones.imprimir_cotizacion', compact('today'));
    // return $pdf->download('imprimir_cotizacion.pdf', array("Attachment" => false));
    return $pdf->stream('imprimir_cotizacion.pdf', array("Attachment" => false));
    //  return "Se va a mostrar Pdf. $id";
    }
}
