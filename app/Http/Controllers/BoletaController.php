<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Boleta;
use App\Models\Cliente;
use App\Models\Producto;

class BoletaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $cotizaciones = Boleta::all();
        // $cotizaciones = Boleta::orderBy('id')->get();

        // $cotizaciones = Boleta::all()->sortBy("id");//Ascendente
        $cotizaciones = Boleta::all()->sortByDesc("id");//Descendente
        $clientes = Cliente::all();
        return view('sistema_cotizaciones.cotizaciones.index',compact('cotizaciones','clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Cliente::all();
        $productos = Producto::all();
        return view('sistema_cotizaciones.cotizaciones.create',compact('clientes','productos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return $request;
        // {"cliente":"2","producto1":"1","cantidad1":"2","producto2":"2","cantidad2":"4","producto3":"4","cantidad3":"10"}
        // return $request;
        //cliente,producto,cantidad
        //fecha,total,cliente_id
        // $fecha = '2020-10-19 00:00:00';

        // --------       
        $fecha = date("Y-m-d H:i:s");
        // $productos = array($request->producto1,$request->producto2,$request->producto3,$request->producto4);
        // $cantidades = array($request->cantidad1,$request->cantidad2,$request->cantidad3,$request->cantidad4);
        $productos = $request->producto;
        $cantidades = $request->cantidad;
        $n = count($productos);
        $total = array();
        $sumatotal = 0;
        $x = 0;

        for($i=0;$i<$n;$i++){
         $un_producto = Producto::find($productos[$i]);
         $total[] = $un_producto->precio * $cantidades[$i];
        }
        $sumatotal = array_sum($total);    

        // if(in_array($x,$cantidades)){
        //    return redirect()->route('cotizaciones.store');
        // }else{
           $boleta = Boleta::create(['fecha'=>$fecha,'total'=>$sumatotal,'cliente_id'=>$request->cliente]);
           for($i=0;$i<$n;$i++){
               if($cantidades[$i] != "0"){
                 $boleta->productos()->attach($productos[$i],['cantidad'=>$cantidades[$i]]);
               }
           }

           return redirect()->route('cotizaciones.index');
        // }
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cotizacion = Boleta::find($id);
        return view('sistema_cotizaciones.cotizaciones.show',compact('cotizacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
