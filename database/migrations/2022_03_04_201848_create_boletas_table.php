<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombres');
            $table->string('apellido_paterno');
            $table->string('apellido_materno');
            $table->integer('tipo_cliente');
            $table->integer('estado');
        });

        Schema::create('boletas', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fecha')->nullable();
            $table->integer('total');
            // $table->unsignedInteger('id_cliente'); // Relación con clientes
            // $table->foreign('id_cliente')->references('id')->on('clientes'); // clave foranea
            $table->foreignId('cliente_id')->nullable()->constrained('clientes')->cascadeOnUpdate()->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
        Schema::dropIfExists('boletas');
    }
}
