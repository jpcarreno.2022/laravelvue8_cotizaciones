<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletaProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boleta_producto', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('boleta_id'); // Relación con boletas
            $table->foreign('boleta_id')->references('id')->on('boletas'); // clave foranea
            $table->unsignedBigInteger('producto_id'); // Relación con productos
            $table->foreign('producto_id')->references('id')->on('productos'); // clave foranea
            $table->integer('cantidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boleta_producto');
    }
}
