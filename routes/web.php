<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
use Barryvdh\DomPDF\Facade\PDF;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\BoletaController;
use App\Http\Controllers\AlumnoMateriaController;
use App\Http\Controllers\ImprimirController;
use App\Http\Controllers\ImprimirPdfController;

use App\Models\Boleta;
use App\Models\Cliente;
use App\Models\Producto;
// use App\Models\Boleta;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sistema_cotizaciones',function(){
    return view('sistema_cotizaciones.clientes.index');
});

Route::get('/administracion',function(){
    return view('sistema_cotizaciones.administracion.index');
})->name('administracion');

Route::get('/crear_usuario',function(){
    return view('sistema_cotizaciones.clientes.create');
})->name('crear_usuario');

Route::get('/boletas',function(){
$boletas = Boleta::all();
// return $boletas;
return view('boletas',compact('boletas'));
});

Route::get('/clientes',function(){
    $clientes = Cliente::all();
    return $clientes;
});

 Route::resource('/clientes',ClienteController::class);

 Route::resource('/productos',ProductoController::class);

 Route::resource('/cotizaciones',BoletaController::class);
//

Route::resource('/alumno_materia',AlumnoMateriaController::class);


Route::get("imprimir_pdf", function (Request $request) {
    $dompdf = App::make("dompdf.wrapper");
    $dompdf->loadView("ejemplo", [
        "nombre" => "Luis Cabrera Benito",
    ]);
    // return $dompdf->stream();
    // return $dompdf->download("mi_archivo.pdf");//Descargar
    $dompdf->save("archivos_pdf/mi_archivo.pdf"); //guardar en carpeta public
    return "PDF guardado";//redirect o save
    // combinadolos
    // return PDF::loadFile(public_path().'/myfile.html')->save('/path-to/my_stored_file.pdf')->stream('download.pdf');
});

// Route::get('/imprimir', GeneradorController::class)-name('print');

Route::get('/pdf',function(){

    $today = "1982";
    $pdf = PDF::loadView('ejemplo2', compact('today'));
    // return $pdf->download('ejemplo2.pdf');
    return $pdf->stream('ejemplo2.pdf');

    // $pdf = PDF::loadView('pdf.invoice', $data);
    // return $pdf->download('invoice.pdf');
});

Route::resource('/imprimir_cotizacion',ImprimirController::class);

Route::get('/mostrarPdf',function(){
$pdf = new ImprimirPdfController();
return $pdf->mostrarPdf($id = 3);
})->name('mostrarPdf');
