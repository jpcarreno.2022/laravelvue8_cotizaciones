@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container-fluid bg-light" style="border:solid 1px #aaa;height:auto;margin-top:5px;margin-left:-10px;">
<br>
<!-- <div class="row"><div class="col col-lg-10"> -->
    <h2>Listado de Productos</h2>
<!-- </div> -->
   <!-- <div class="col col-lg-2"><a href="" class="btn btn-primary text-light">+ Nuevo</a></div></div> -->
    <hr>
    <br>
    <table id="tblProductos" class="table table-hover table-striped text-center">
        <thead>
            <tr>
                <th>#</th>
                <th>DESCRIPCIÓN</th>
                <th>PRECIO</th>
                <th>STOCK</th>
                <th>ESTADO</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @php
            $i = 0
        @endphp
    @foreach($productos as $prod)
    <tr>
        <td>{{++$i}}</td>
        <td>{{$prod->descripcion}}</td>
        <td>{{$prod->precio}}</td>
        <td>{{$prod->stock}}</td>

        @if($prod->estado == 0)
        <td><p class="text-danger">{{'DisContinuado'}}</p></td>
        @elseif($prod->estado == 1)
        <td><p class="text-success">{{'Activo'}}</p></td>
        @endif
        <td><a href="{{route('productos.edit', $prod->id)}}" class="btn btn-warning text-light">Editar</a></td>
        <td>
            <form method="POST" action="{{route('productos.destroy', $prod->id)}}" role="form">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">

                @if($prod->estado == 0)
                <button type="submit" class="btn btn-success text-light">{{'Habilitar'}}</button>
                @elseif($prod->estado == 1)
                <button type="submit" class="btn btn-danger text-light">{{'DesHabilitar'}} </button>
                 @endif

            </form>
        </td>
    </tr>
    @endforeach
</tbody>
    </table>

</div>
@endsection




