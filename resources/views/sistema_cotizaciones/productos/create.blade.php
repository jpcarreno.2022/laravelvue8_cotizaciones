@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container-fluid">
    <h1>Crear Producto</h1>
    <hr>

<form method="POST" action="{{ route('productos.store') }}"  role="form" accept-charset="UTF-8" enctype="multipart/form-data">
{{ csrf_field() }}
<div>
    <label for="descripcion">Descripción:</label>
    <input type="text" class="form form-control" name="descripcion" placeholder="Descripción"/>
</div>
<br>
<div>
    <label for="precio">Precio:</label>
    <input type="text" class="form form-control" name="precio" placeholder="Precio"/>
</div>
<br>
<div>
    <label for="stock">Stock:</label>
    <input type="text" class="form form-control" name="stock" placeholder="Stock"/>
</div>
<br>
<div>
    <label for="archivo"><b>Archivo: </b></label>
    <input type="file" name="archivo" required class="form form-control">
</div>
    <br>
<div>
    <label for="estado">Estado Producto:</label>
    <select name="estado" class="form form-control">
        <option value="1">1 - Activo</option>
        <option value="0">0 - DisContinuado</option>
    </select>
</div>
<br>
<div>
<input type="submit"  value="Guardar" class="btn btn-primary form-control"/>
    <!-- <button type="submit" class="btn btn-primary form-control">Enviar</button> -->
</div>

</form>
</div>
@endsection


