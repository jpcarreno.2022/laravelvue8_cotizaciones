@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container-fluid">
    <h1>Editar Producto</h1>
    <hr>

<form method="POST" action="{{route('productos.update',$producto->id)}}"  role="form">
{{ csrf_field() }}
@method('PUT')
<div>
    <label for="descripcion">Descripción:</label>
    <input type="text" class="form form-control" name="descripcion" placeholder="Descripción" value="{{$producto->descripcion}}"/>
</div>

<div>
    <label for="precio">Precio:</label>
    <input type="text" class="form form-control" name="precio" placeholder="Precio" value="{{$producto->precio}}"/>
</div>

<div>
    <label for="stock">Stock:</label>
    <input type="text" class="form form-control" name="stock" placeholder="Stock" value="{{$producto->stock}}"/>
</div>

<div>
    <label for="estado">Estado Producto:</label>
    <select name="estado" class="form form-control">
        @if($producto->estado == 0)
        <option value="0">0 - DisContinuado</option>
        <option value="1">1 - Activo</option>
        @elseif($producto->estado == 1)
        <option value="1">1 - Activo</option>
        <option value="0">0 - DisContinuado</option>
        @endif
    </select>
</div>
<br>
<div>
<input type="submit"  value="Guardar" class="btn btn-primary form-control"/>
    <!-- <button type="submit" class="btn btn-primary form-control">Enviar</button> -->
</div>

</form>
</div>
@endsection
