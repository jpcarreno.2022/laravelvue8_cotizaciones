<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <style>
        h1{
        text-align: center;
        text-transform: uppercase;
        }
        .contenido{
        font-size: 16px;
        }

    </style>
    </head>
    <body>
        <h1>COTIZACIÓN</h1>
        <hr>

<div class="contenido">
    <div class="container bg-light" style="border:solid 1px #aaa;height:550px;margin-top:5px;margin-left:-10px;padding:10px;">
<br>
    <h3>Detalle de la Cotización #{{$cotizacion->id}}</h3>
    <hr>
        <div style="border:1px solid #ccc;width:100%;">
            <table Id="tblCotizacionPdf" class="table table-hover table-bordered table-striped text-center">
                    <tr>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th>Prec. Unit</th>
                        <th>Cant.</th>
                        <th>Total</th>
                    </tr>
                    @foreach($cotizacion->productos as $prod)
                    @php
                        $x = rand(00005, 00015)
                    @endphp
                       <tr>
                           <td width="70" align="center">{{$x}}{{$cotizacion->id}}</td><!-- 23400{{$prod->id}}-->
                           <td width="190" align="center">{{$prod->descripcion}}</td>
                           <td width="100" align="center">{{$prod->precio}}</td>
                           <td width="60"  align="center">{{$prod->pivot->cantidad}}</td>
                           <td width="80" align="center">{{$prod->pivot->cantidad*$prod->precio}} </td>
                        </tr>
<!-- {{$cotizacion->total}} -->
                   @endforeach
                   <!-- <tr style="margin-top:20px;"><td colspan="4">$/Total</td><td>{{$cotizacion->total}}</td></tr> -->
            </table>
            <hr>
            <br><br><br>
            <div style="border:solid 1px #ccc;">
            <table Id="tblTotal" class="table table-hover table-bordered table-striped">
                <tr style="margin-top:20px;font-size:20px;font-weight:bold;">
                    <td width="300">&nbsp;&nbsp;&nbsp;$/Total</td><td width="150"></td><td  style="color:red;">{{$cotizacion->total}}</td>
                </tr>
            </table>
            </div>
        </div>
    <br>
   </div>
    <br>
<center><p><b>*** GRACIAS POR SU PREFERENCIA ***</b></p></center>


            <!-- <p id="primero">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore nihil illo odit aperiam alias rem voluptatem odio maiores doloribus facere recusandae suscipit animi quod voluptatibus, laudantium obcaecati quisquam minus modi.</p> -->
            <!-- <p id="segundo">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore nihil illo odit aperiam alias rem voluptatem odio maiores doloribus facere recusandae suscipit animi quod voluptatibus, laudantium obcaecati quisquam minus modi.</p> -->
            <!-- <p id="tercero">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore nihil illo odit aperiam alias rem voluptatem odio maiores doloribus facere recusandae suscipit animi quod voluptatibus, laudantium obcaecati quisquam minus modi.</p> -->
        </div>

<style>
    #tblCotizacionPdf th{
        background-color:#aaa;
    }
    #tblCotizacionPdf td{

    }
</style>
    </body>
</html>
