@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container-fluid bg-light" style="border:solid 1px #aaa;height:auto;margin-top:5px;margin-left:-10px;">
<br>
    <h2>Listado de Cotizaciones</h2>

    <hr>
    <br>
    <table id="tblCotizaciones" class="table table-hover table-striped text-center" style="font-size:14px;">
        <thead>
            <tr>
                <th>#</th>
                <th>N° COTIZACIÓN</th>
                <th>FECHA COTIZACIÓN</th>
                <th>MONTO TOTAL</th>
                <th>CLIENTE</th>
                <th>DETALLE COTIZACIÓN</th>

            </tr>
        </thead>
        <tbody>
        @php
            $i = 0
        @endphp
            @foreach($cotizaciones as $cot)
            <tr>
                <td>{{++$i}}</td>
                <td>{{$cot->id}}</td>
                <td>{{$cot->fecha}}</td>
                <td>{{$cot->total}}</td>
                @foreach($clientes as $clie)
                @if($clie->id == $cot->cliente_id)
                <!-- <td>{{$cot->cliente_id}}</td> -->
                <td>{{$clie->nombres}}&nbsp;{{$clie->apellido_paterno}}&nbsp;{{$clie->apellido_materno}}</td>
                @endif
                @endforeach
                <td>
                <a href="{{route('cotizaciones.show',$cot->id)}}"><i class="fa fa-file-text fa-2x text-success" title="Ver detalle cotización #{{$cot->id}}"></i></a>
                    <!-- <a id="{{$cot->id}}" class="abrirmodal"><i class="fa fa-file-text fa-2x text-success" title="Ver detalle cotización #{{$cot->id}}"></i></a> -->
                </td>
            @endforeach
        </tbody>
    </table>
</div>


</tr>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Fin Modal -->






<script>
$( document ).ready(function() {
    $("#exampleModal").modal('hide');
$(".abrirmodal").on("click",function(){
     var modalId = this.id;
     alert(modalId);
    $(".modal-title").html('Detalle de la Cotización #'+modalId);
    $(".modal-body").html(`<div><table Id="tblDetalle" class="table table-hover table-condensed"><thead><tr><th>Código</th><th>Descripción</th><th>$/U</th><th>Cantidad</th><th>$/Total</th><tr></thead><tbody>@foreach($cotizaciones as $cot)@if($cot->id)<tr><td>{{$cot->id}}<td><td></td><td></td><td></td><td></td></tr> @endif @endforeach</tbody></table></div>`);

    $("#exampleModal").modal('show');
})
});
</script>


@endsection




