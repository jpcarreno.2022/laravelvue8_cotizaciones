@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container-fluid">
    <h1>Crear Cotización</h1>
    <hr>

<form method="POST" action="{{ route('cotizaciones.store') }}"  role="form">
{{ csrf_field() }}
<div>
    <label for="cliente">Cliente:</label>
    <select name="cliente" class="form form-control">
        @foreach($clientes as $clie)
          <option value="{{$clie->id}}">  {{$clie->id}} &nbsp; {{$clie->nombres}} &nbsp; {{$clie->apellido_paterno}}</option>
        @endforeach
    </select>

    <!-- <input type="text" class="form form-control" name="nombres" placeholder="Nombres"/> -->
</div>

<div>
    <label for="producto">Agregar Productos:</label>
    <select name="producto" class="form form-control">
    @foreach($productos as $prod)
          <option value="{{$prod->id}}">{{$prod->id}}&nbsp;{{$prod->descripcion}} &nbsp; {{$prod->precio}} &nbsp; {{$prod->stock}}</option>
        @endforeach
    </select>
    <!-- <input type="text" class="form form-control" name="apellido_paterno" placeholder="Apelllido Paterno"/> -->
</div>

<div>
    <label for="cantidad">Cantidad:</label>
    <input type="text" class="form form-control" name="cantidad" placeholder="cantidad"/>
</div>
<br>
<div>
<input type="submit"  value="Guardar" class="btn btn-primary form-control"/>
    <!-- <button type="submit" class="btn btn-primary form-control">Enviar</button> -->
</div>

</form>
</div>
@endsection
