@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container-fluid">
    <h1>Crear Cotización</h1>
    <hr>
<p id=""></p>
    <form method="POST" action="{{ route('cotizaciones.store') }}"  role="form">
    {{ csrf_field() }}
    <div>
        <label for="cliente"><b>Cliente:</b></label>
        <select name="cliente" class="form form-control">
            @foreach($clientes as $clie)
              <option value="{{$clie->id}}">  {{$clie->id}} &nbsp; {{$clie->nombres}} &nbsp; {{$clie->apellido_paterno}}</option>
            @endforeach
        </select>
        <!-- <input type="text" class="form form-control" name="nombres" placeholder="Nombres"/> -->
    </div>
    <br>
        <div><label for="producto"><b>Agregar Productos:</b></label>
    </div>

    <div class="row" style="border:solid 1px #aaa;padding:10px;">

        @foreach($productos as $prod)
        <div class="col col-lg-4">
            <div class="card bg-warning mb-2 mt-3">
                <!-- <img src="/storage/producto{{$prod->id}}.jpg" alt="img" height="300"/> -->
                <img src="/storage/{{$prod->imagen}}" alt="img" height="300"/>
              <div class="card-body">
              <label for="producto{{$prod->id}}"><b>Producto #{{$prod->id}}:</b></label>
              <p style="background-color:#fff;">{{$prod->id}}&nbsp;{{$prod->descripcion}} &nbsp; {{$prod->precio}} &nbsp; {{$prod->stock}}</p>
              <input type="hidden" name="producto[]" class="form form-control" value="{{$prod->id}}"/>              
                    <br>
                    <label for="cantidad{{$prod->id}}">Cantidad:{{$prod->id}}</label>
                    <input type="number" class="form form-control" name="cantidad[]" placeholder="0" value="0"/>
              </div>
            </div>
        </div>
         @endforeach




<!--  -->

  <!-- <select name="producto1" class="form form-control"> -->
                      <!-- @foreach($productos as $prod) -->
                      <!-- <option value="{{$prod->id}}">{{$prod->id}}&nbsp;{{$prod->descripcion}} &nbsp; {{$prod->precio}} &nbsp; {{$prod->stock}}</option> -->
                      <!-- @endforeach -->
                <!-- </select> -->
        <!-- <div class="col col-lg-4"> -->
            <!-- <div class="card bg-warning"> -->
              <!-- <div class="card-body"> -->
              <!-- <label for="producto1"><b>Producto #1:</b></label> -->
                <!-- <select name="producto1" class="form form-control"> -->
                      <!-- @foreach($productos as $prod) -->
                      <!-- <option value="{{$prod->id}}">{{$prod->id}}&nbsp;{{$prod->descripcion}} &nbsp; {{$prod->precio}} &nbsp; {{$prod->stock}}</option> -->
                      <!-- @endforeach -->
                <!-- </select> -->
                    <!-- <br> -->
                    <!-- <label for="cantidad1">Cantidad:</label> -->
                    <!-- <input type="number" class="form form-control" name="cantidad1" value="0"/> -->
              <!-- </div> -->
            <!-- </div> -->
        <!-- </div> -->
<!--  -->
        <!-- <div class="col col-lg-4"> -->
            <!-- <div class="card bg-warning"> -->
              <!-- <div class="card-body"> -->
              <!-- <label for="producto2"><b>Producto #2:</b></label> -->
                <!-- <select name="producto2" class="form form-control"> -->
                      <!-- @foreach($productos as $prod) -->
                      <!-- <option value="{{$prod->id}}">{{$prod->id}}&nbsp;{{$prod->descripcion}} &nbsp; {{$prod->precio}} &nbsp; {{$prod->stock}}</option> -->
                      <!-- @endforeach -->
                <!-- </select> -->
                    <!-- <br> -->
                    <!-- <label for="cantidad2">Cantidad:</label> -->
                    <!-- <input type="number" class="form form-control" name="cantidad2" value="0"/> -->
              <!-- </div> -->
            <!-- </div> -->
        <!-- </div> -->
<!--  -->
        <!-- <div class="col col-lg-4"> -->
            <!-- <div class="card bg-warning"> -->
              <!-- <div class="card-body"> -->
              <!-- <label for="producto3"><b>Producto #3:</b></label> -->
                <!-- <select name="producto3" class="form form-control"> -->
                      <!-- @foreach($productos as $prod) -->
                      <!-- <option value="{{$prod->id}}">{{$prod->id}}&nbsp;{{$prod->descripcion}} &nbsp; {{$prod->precio}} &nbsp; {{$prod->stock}}</option> -->
                      <!-- @endforeach -->
                <!-- </select> -->
                    <!-- <br> -->
                    <!-- <label for="cantidad3">Cantidad:</label> -->
                    <!-- <input type="number" class="form form-control" name="cantidad3" value="0"/> -->
              <!-- </div> -->
            <!-- </div> -->
        <!-- </div> -->


<!--  -->
    </div>


<br>
<div>
<input type="submit"  value="Guardar" class="btn btn-primary form-control"/>
    <!-- <button type="submit" class="btn btn-primary form-control">Enviar</button> -->
</div>

</form>
</div>
@endsection



<!-- <div class="col col-lg-4 form form-control"> -->
        <!-- <select name="producto" class="form form-control"> -->
        <!-- @foreach($productos as $prod) -->
              <!-- <option value="{{$prod->id}}">{{$prod->id}}&nbsp;{{$prod->descripcion}} &nbsp; {{$prod->precio}} &nbsp; {{$prod->stock}}</option> -->
            <!-- @endforeach -->
        <!-- </select> -->
<!--  -->
<!--  -->
        <!-- <label for="cantidad">Cantidad:</label> -->
        <!-- <input type="text" class="form form-control" name="cantidad" placeholder="cantidad"/> -->
    <!-- </div> -->
    <!-- <div class="col col-lg-4 form form-control"> -->
        <!-- <select name="producto" class="form form-control"> -->
        <!-- @foreach($productos as $prod) -->
              <!-- <option value="{{$prod->id}}">{{$prod->id}}&nbsp;{{$prod->descripcion}} &nbsp; {{$prod->precio}} &nbsp; {{$prod->stock}}</option> -->
            <!-- @endforeach -->
        <!-- </select> -->
<!--  -->
<!--  -->
        <!-- <label for="cantidad">Cantidad:</label> -->
        <!-- <input type="text" class="form form-control" name="cantidad" placeholder="cantidad"/> -->
    <!-- </div> -->
    <!-- <div class="col col-lg-4 form form-control"> -->
        <!-- <select name="producto" class="form form-control"> -->
        <!-- @foreach($productos as $prod) -->
              <!-- <option value="{{$prod->id}}">{{$prod->id}}&nbsp;{{$prod->descripcion}} &nbsp; {{$prod->precio}} &nbsp; {{$prod->stock}}</option> -->
            <!-- @endforeach -->
        <!-- </select> -->
      <!--  -->
        <!-- <label for="cantidad">Cantidad:</label> -->
        <!-- <input type="text" class="form form-control" name="cantidad" placeholder="cantidad"/> -->
    <!-- </div> -->
<!--  -->
