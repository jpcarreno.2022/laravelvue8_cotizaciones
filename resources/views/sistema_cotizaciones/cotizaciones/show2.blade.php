@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container bg-light" style="border:solid 1px #aaa;height:550px;margin-top:5px;margin-left:-10px;">
<br>
    <h2>Detalle de la Cotización #{{$cotizacion->id}}</h2>
    <hr>
    <div style="border:1px solid #ccc;">
        <table Id="tblDetalle" class="table table-hover table-bordered table-striped">
                <tr><th>Código Producto</th><td>23400{{$cotizacion->id}}</td></tr>
                    @foreach($cotizacion->productos as $prod)
                        <tr><th>Descripción</th><td>{{$prod->descripcion}}</td></tr>
                        <tr><th>$/U</th><td>{{$prod->precio}}</td></tr>
                        <tr><th>Cantidad</th><td>{{$prod->pivot->cantidad}}</td></tr>
                    @endforeach
                <tr><th>$/Total</th><td>{{$cotizacion->total}}</td></tr>
        </table>
    </div>
    <br>
    <div class="row"><div class="col col-lg-12">
        <form action="{{route('imprimir_cotizacion.store',$cotizacion->id)}}" method="POST" role="form">
            {{csrf_field()}}
        <!-- <a href="{{route('imprimir_cotizacion.index')}}" class="btn btn-primary form form-control" target="_blank">Imprimir Cotización</a> -->
        <!-- <a class="btn btn-primary form form-control" target="_blank"><button type="submit">Imprimir Cotización</button></a> -->
        <input type="hidden" name="idCotizacion" id="idCotizacion" value="{{$cotizacion->id}}">
        <a><button type="submit" class="btn btn-primary form form-control" formtarget="_blank">Imprimir Cotización</button></a>
    </form>
    </div></div>
    <br>
</div>
@endsection
<style>
    #tblDetalle th{
        background-color:#aaa;
        width: 200px;
    }
</style>
