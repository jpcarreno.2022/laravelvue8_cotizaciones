@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container bg-light" style="border:solid 1px #aaa;height:550px;margin-top:5px;margin-left:-10px;">
<br>
    <h2>Detalle de la Cotización #{{$cotizacion->id}}</h2>
    <hr>
    <div style="border:1px solid #ccc;">
        <table Id="tblDetalle" class="table table-hover table-bordered table-striped">
                <tr><th>Código Producto</th><th>Descripción</th><th>$/U</th><th>Cantidad</th><th>Total</th></tr>
                    @foreach($cotizacion->productos as $prod)
                    @php
                        $x = rand(23005, 23200);
                        $y = rand(123,199);
                    @endphp
                    <tr>
                        <td>{{$x}}{{$y}}{{$cotizacion->id}}</td>
                        <td>{{$prod->descripcion}}</td>
                        <td align="center">{{$prod->precio}}</td>
                        <td align="center">{{$prod->pivot->cantidad}}</td>
                        <td align="center">{{$prod->pivot->cantidad*$prod->precio}}
                    </tr>
                    @endforeach
        </table>
        <br>
        <table class="table table-bordered">
              <tr style="height:50px;">
                <td width="200"></td>
                <td width="200"></td>
                <td width="200"></td>
                <th align="left" width="200" style="font-size:20px;color:black;background:#999D9C;">$/Total</th>
                <td align="center" style="font-size:20px;color:red;font-weight:bold;">{{$cotizacion->total}}</td>
               </tr>
        </table>
    </div>
    <br>
    <div class="row"><div class="col col-lg-12">
        <form action="{{route('imprimir_cotizacion.store',$cotizacion->id)}}" method="POST" role="form">
            {{csrf_field()}}
        <!-- <a href="{{route('imprimir_cotizacion.index')}}" class="btn btn-primary form form-control" target="_blank">Imprimir Cotización</a> -->
        <!-- <a class="btn btn-primary form form-control" target="_blank"><button type="submit">Imprimir Cotización</button></a> -->
        <input type="hidden" name="idCotizacion" id="idCotizacion" value="{{$cotizacion->id}}">
        <a><button type="submit" class="btn btn-primary form form-control" formtarget="_blank">Imprimir Cotización</button></a>
    </form>
    </div></div>
    <br>
</div>
@endsection
<style>
    #tblDetalle th{
        background-color:#aaa;
        width: 200px;
        text-align:center;
    }
</style>
