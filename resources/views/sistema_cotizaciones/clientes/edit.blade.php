@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container-fluid"style="border:solid 1px #aaa;height:550px;margin-top:5px;margin-left:-10px;">
<br>
    <h2>Editar Cliente</h2>
    <hr>

<form method="POST" action="{{ route('clientes.update',$cliente->id) }}"  role="form">
{{ csrf_field() }}
@method('PUT')
<div>
    <label for="nombres">Nombres:</label>
    <input type="text" class="form form-control" name="nombres" value="{{$cliente->nombres}}" placeholder="Nombres"/>
</div>
<br>
<div>
    <label for="apellido_paterno">Apellido Paterno:</label>
    <input type="text" class="form form-control" name="apellido_paterno" value="{{$cliente->apellido_paterno}}" placeholder="Apelllido Paterno"/>
</div>
<br>
<div>
    <label for="apellido_materno">Apellido Materno:</label>
    <input type="text" class="form form-control" name="apellido_materno" value="{{$cliente->apellido_materno}}" placeholder="Apelllido Materno"/>
</div>
<br>
<div>
    <label for="tipo_cliente">Tipo Cliente:</label>
    <select name="tipo_cliente" class="form form-control">
        <option value="1">1 - Particular</option>
        <option value="2">2 - Empresa</option>
    </select>
</div>
<br>

<div>
    <label for="estado">Estado Cliente:</label>
    <select name="estado" class="form form-control">
        @if($cliente->estado == 0)
            <option value="0">0 - DesHabilitado</option>
            <option value="1">1 - Habilitado</option>
        @elseif($cliente->estado == 1)
            <option value="1">1 - Habilitado</option>
            <option value="0">0 - DesHabilitado</option>
        @endif
    </select>
</div>
<br>

<div>
<input type="submit"  value="Guardar" class="btn btn-primary form-control"/>
    <!-- <button type="submit" class="btn btn-primary form-control">Enviar</button> -->
</div>

</form>
</div>
@endsection
