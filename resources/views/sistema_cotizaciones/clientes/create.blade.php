@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container-fluid" style="border:1px solid #ccc;height:auto;">
    <h1>Crear Cliente</h1>
    <hr>

<form method="POST" action="{{ route('clientes.store') }}"  role="form">
{{ csrf_field() }}

    <div class="row">
            <div class="col col-lg-6">
                <label for="rut"><b>Rut:</b></label>
                <input type="text" class="form form-control" name="rut" placeholder="Rut sin puntos ni guión - Ej. 12345678K"/>
            </div>
            <div class="col col-lg-6">
                <label for="tipo_cliente"><b>Tipo Cliente:</b></label>
                <select name="tipo_cliente" class="form form-control">
                    <option value="1">1 - Particular</option>
                    <option value="2">2 - Empresa</option>
                </select>
            </div>
    </div>

    <br>

    <div class="row">
        <div class="col col-lg-6">
            <label for="nombres"><b>Nombres:</b></label>
            <input type="text" class="form form-control" name="nombres" placeholder="Nombres"/>
        </div>
        <!-- <div class="col col-lg-6"> -->
            <!-- <label for="rut"><b>Rut:</b></label> -->
            <!-- <input type="text" class="form form-control" name="rut" placeholder="Rut sin puntos ni guión"/> -->
        <!-- </div> -->
    </div>
<br>
    <div class="row">
        <div class="col col-lg-6">
            <label for="apellido_paterno"><b>Apellido Paterno:</b></label>
            <input type="text" class="form form-control" name="apellido_paterno" placeholder="Apelllido Paterno"/>
        </div>
        <!-- <div class="col col-lg-6"> -->
            <!-- <label for="apellido_materno"><b>Apellido Materno:</b></label> -->
            <!-- <input type="text" class="form form-control" name="apellido_materno" placeholder="Apelllido Materno"/> -->
        <!-- </div> -->
    </div>
<br>
    <div class="row">
        <div class="col col-lg-6">
            <label for="apellido_materno"><b>Apellido Materno:</b></label>
            <input type="text" class="form form-control" name="apellido_materno" placeholder="Apelllido Materno"/>
        </div>
        <!-- <div class="col col-lg-6"> -->
            <!-- <label for="tipo_cliente"><b>Tipo Cliente:</b></label> -->
            <!-- <select name="tipo_cliente" class="form form-control"> -->
                <!-- <option value="1">1 - Particular</option> -->
                <!-- <option value="2">2 - Empresa</option> -->
            <!-- </select> -->
        <!-- </div>     -->
        <!-- <div class="col col-lg-6"> -->
            <!-- <label for="estado"><b>Estado Cliente:</b></label> -->
            <!-- <select name="estado" class="form form-control"> -->
                <!-- <option value="1">1 - Habilitado</option> -->
                <!-- <option value="0">0 - DesHabilitado</option> -->
            <!-- </select> -->
        <!-- </div> -->
    </div>

    <br>
    <div class="row">
        <!-- <div class="col col-lg-6"> -->
            <!-- <label for="tipo_cliente"><b>Tipo Cliente:</b></label> -->
            <!-- <select name="tipo_cliente" class="form form-control"> -->
                <!-- <option value="1">1 - Particular</option> -->
                <!-- <option value="2">2 - Empresa</option> -->
            <!-- </select> -->
        <!-- </div>     -->
        <div class="col col-lg-6">
            <label for="estado"><b>Estado Cliente:</b></label>
            <select name="estado" class="form form-control">
                <option value="1">1 - Habilitado</option>
                <option value="0">0 - DesHabilitado</option>
            </select>
        </div>
    </div>


    <br>

    <div class="row">
        <input type="submit"  value="Guardar" class="btn btn-primary form-control"/>
        <!-- <button type="submit" class="btn btn-primary form-control">Enviar</button> -->
    </div>

    <br>

</form>
</div>
@endsection
