@extends('sistema_cotizaciones.main.app2')
@section('content')
<div class="container-fluid bg-light" style="border:solid 1px #aaa;height:550px;margin-top:5px;margin-left:-10px;">
<br>
    <h2>Listado de Clientes</h2>
    <hr>
    <br>
    <table id="tblClientes" class="table table-hover table-striped text-center">
        <thead>
            <tr>
                <th>#</th>
                <th>NOMBBRES</th>
                <th>APELLIDO PATERNO</th>
                <th>APELLIDO MATERNO</th>
                <th>TIPO CLIENTE</th>
                <th>ESTADO</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @php
            $i = 0
        @endphp
    @foreach($clientes as $clie)
    <tr>
        <td>{{++$i}}</td>
        <td>{{$clie->nombres}}</td>
        <td>{{$clie->apellido_paterno}}</td>
        <td>{{$clie->apellido_materno}}</td>

        @if($clie->tipo_cliente == 1)
        <td>{{'Particular'}}</td>
        @elseif($clie->tipo_cliente == 2)
        <td>{{'Empresa'}}</td>
        @endif

        @if($clie->estado == 0)
        <td><p class="text-danger">{{'DesHabilitado'}}</p></td>
        @elseif($clie->estado == 1)
        <td><p class="text-success">{{'Habilitado'}}</p></td>
        @endif
        <td><a href="{{route('clientes.edit', $clie->id)}}" class="btn btn-warning text-light">Editar</a></td>
        <td>
            <form method="POST" action="{{route('clientes.destroy', $clie->id)}}" role="form">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">

                @if($clie->estado == 0)
                <button type="submit" class="btn btn-success text-light">{{'Habilitar'}}</button>
                @elseif($clie->estado == 1)
                <button type="submit" class="btn btn-danger text-light">{{'DesHabilitar'}} </button>
                 @endif

            </form>
        </td>
    </tr>
    @endforeach
</tbody>
    </table>

</div>
@endsection




