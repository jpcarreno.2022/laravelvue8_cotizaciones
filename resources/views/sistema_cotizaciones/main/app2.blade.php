<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" value="{{ csrf_token() }}"/>
    <title>Laravel 8 Menú</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css"
    />

<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script
      type="text/javascript"
      src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
    ></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script
      type="text/javascript"
      charset="utf8"
      src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"
    ></script>
<style>
    .col-sm-3{
      /* height: 150px;
        background-color:#01549b;*/
}
.col-sm-9{
    height: 630px;
    /* background-color:#2aabd2; */
    border: 1px solid #ccc;
    opacity: 0.9;
}


.nav-side-menu {
    overflow: auto;
    font-family: verdana;
    font-size: 12px;
    font-weight: 200;
    background-color: #2e353d;
    position: fixed;
    top: 0px;
    /* width: 24%; */
    width: 15%;
    height: 100%;
    color: #e1ffff;
}
.nav-side-menu .brand {
    background-color: #23282e;
    line-height: 50px;
    display: block;
    text-align: center;
    font-size: 14px;
}
.nav-side-menu .toggle-btn {
    display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
    list-style: none;
    padding: 0px;
    margin: 0px;
    line-height: 35px;
    cursor: pointer;

     /* .collapsed{
         .arrow:before{
                   font-family: FontAwesome;
                   content: "\f053";
                   display: inline-block;
                   padding-left:10px;
                   padding-right: 10px;
                   vertical-align: middle;
                   float:right;
              }
       }*/

}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
    font-family: FontAwesome;
    content: "\f078";
    display: inline-block;
    padding-left: 10px;
    padding-right: 10px;
    vertical-align: middle;
    float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
    border-left: 3px solid #d19b3d;
    background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
    color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
    color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
    background-color: #181c20;
    border: none;
    line-height: 28px;
    border-bottom: 1px solid #23282e;
    margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
    background-color: #020203;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
    font-family: FontAwesome;
    content: "\f105";
    display: inline-block;
    padding-left: 10px;
    padding-right: 10px;
    vertical-align: middle;
}
.nav-side-menu li {
    padding-left: 0px;
    border-left: 3px solid #2e353d;
    border-bottom: 1px solid #23282e;
}
.nav-side-menu li a {
    text-decoration: none;
    color: #e1ffff;
}
.nav-side-menu li a i {
    padding-left: 10px;
    width: 20px;
    padding-right: 20px;
}
.nav-side-menu li:hover {
    border-left: 3px solid #d19b3d;
    background-color: #4f5b69;
    -webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;
}
@media (max-width: 767px) {
    .nav-side-menu {
        position: relative;
        width: 100%;
        margin-bottom: 10px;
    }
    .nav-side-menu .toggle-btn {
        display: block;
        cursor: pointer;
        position: absolute;
        right: 10px;
        top: 10px;
        z-index: 10 !important;
        padding: 3px;
        background-color: #ffffff;
        color: #000;
        width: 40px;
        text-align: center;
    }
    .brand {
        text-align: left !important;
        font-size: 22px;
        padding-left: 20px;
        line-height: 50px !important;
    }
}
@media (min-width: 767px) {
    .nav-side-menu .menu-list .menu-content {
        display: block;
    }
}
body {
    margin: 0px;
    padding: 0px;
}

</style>
</head>
<body>


        <div  class="container-fluid">
            <div class="row">
                <div class="col-sm-2">
                    <div class="nav-side-menu">
                        <div class="brand">Brand Logo</div>
                        <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
                        <div class="menu-list">
                            <ul id="menu-content" class="menu-content collapse out">
                                <li>
                                    <a href="{{route('administracion')}}">
                                        <i class="fa fa-dashboard fa-lg"></i> Dashboard
                                    </a>
                                </li>
<!--  -->
                                <li data-toggle="collapse" data-target="#new" class="collapsed">
                                    <a><i class="fa fa-users fa-lg"></i> Clientes <span class="arrow"></span></a>
                                </li>
                                <ul class="sub-menu collapse" id="new">
                                    <li><a href="{{route('clientes.index')}}">Listado de Clientes</a></li>
                                    <li><a href="{{route('clientes.create')}}">Crear Cliente</a></li>
                                    <li>Editar Cliente</li>
                                    <li>Buscar Cliente</li>
                                </ul>

                                <!--  -->
                                <li data-toggle="collapse" data-target="#productos" class="collapsed">
                                    <a href="#"><i class="fa fa-desktop fa-lg"></i>&nbsp;Productos<span class="arrow"></span></a>
                                </li>
                                <ul class="sub-menu collapse" id="productos">
                                    <li><a href="{{route('productos.index')}}">Listado de Productos</a></li>
                                    <li><a href="{{route('productos.create')}}">Crear  Producto</a></li>
                                    <li>Editar Producto</li>
                                    <li>Buscar Producto</li>
                                </ul>

                                <!--  -->
                                <li data-toggle="collapse" data-target="#cotizacion" class="collapsed">
                                    <a href="#"><i class="fa fa-files-o fa-lg"></i> Cotizaciones <span class="arrow"></span></a>
                                </li>
                                <ul class="sub-menu collapse" id="cotizacion">
                                    <li><a href="{{route('cotizaciones.index')}}">Listado de Cotizaciones</a></li>
                                    <li><a href="{{route('cotizaciones.create')}}">Crear Cotización</a></li>
                                    <li>Editar Cotización</li>
                                    <li>Buscar Cotización</li>
                                </ul>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-1">
                    @yield('content')
                </div>
            </div>
        </div>


<script>
     $("#tblClientes").dataTable({
          "language": {
        "decimal":        ".",
        "emptyTable":     "<stron style='color:#000;'>No hay datos para mostrar</strong>",
        "info":           "del _START_ al _END_ (_TOTAL_ total)",
        "infoEmpty":      "del 0 al 0 (0 total)",
        "infoFiltered":   "(filtrado de todas las _MAX_ entradas)",
        "infoPostFix":    "",
        "thousands":      "'",
        "lengthMenu":     "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing":     "Procesando...",
        "search":         "Buscar:",
        "zeroRecords":    "No hay resultados",
        "paginate": {
          "first":      "Primero",
          "last":       "Último",
          "next":       "Siguiente",
          "previous":   "Anterior"
        },
        "aria": {
          "sortAscending":  ": ordenar de manera Ascendente",
          "sortDescending": ": ordenar de manera Descendente ",
        }
      }
     });

     $("#tblProductos").dataTable({
          "language": {
        "decimal":        ".",
        "emptyTable":     "<stron style='color:#000;'>No hay datos para mostrar</strong>",
        "info":           "del _START_ al _END_ (_TOTAL_ total)",
        "infoEmpty":      "del 0 al 0 (0 total)",
        "infoFiltered":   "(filtrado de todas las _MAX_ entradas)",
        "infoPostFix":    "",
        "thousands":      "'",
        "lengthMenu":     "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing":     "Procesando...",
        "search":         "Buscar:",
        "zeroRecords":    "No hay resultados",
        "paginate": {
          "first":      "Primero",
          "last":       "Último",
          "next":       "Siguiente",
          "previous":   "Anterior"
        },
        "aria": {
          "sortAscending":  ": ordenar de manera Ascendente",
          "sortDescending": ": ordenar de manera Descendente ",
        }
      }
     });

     $("#tblCotizaciones").dataTable({
          "language": {
        "decimal":        ".",
        "emptyTable":     "<stron style='color:#000;'>No hay datos para mostrar</strong>",
        "info":           "del _START_ al _END_ (_TOTAL_ total)",
        "infoEmpty":      "del 0 al 0 (0 total)",
        "infoFiltered":   "(filtrado de todas las _MAX_ entradas)",
        "infoPostFix":    "",
        "thousands":      "'",
        "lengthMenu":     "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing":     "Procesando...",
        "search":         "Buscar:",
        "zeroRecords":    "No hay resultados",
        "paginate": {
          "first":      "Primero",
          "last":       "Último",
          "next":       "Siguiente",
          "previous":   "Anterior"
        },
        "aria": {
          "sortAscending":  ": ordenar de manera Ascendente",
          "sortDescending": ": ordenar de manera Descendente ",
        }
      }
     });
</script>

</body>
</html>
