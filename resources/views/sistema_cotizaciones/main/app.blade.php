<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" value="{{ csrf_token() }}"/>
    <title>Laravel 8 Menú</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css"
    />
    <script
      type="text/javascript"
      src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
    ></script>
    <script
      type="text/javascript"
      charset="utf8"
      src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"
    ></script>

</head>
<body>


<div class="container-fluid" style="width:100%;height:auto;background-color:#000;">

    <!-- header -->
    <div class="row bg-primary text-light"><h2>ESTE ES EL ENCABEZADO</h2></div>

    <div class="row" style="height:560px;width:100%;">

    <!-- Aquí va el menú -->
        <div class="col col-lg-2 bg-secondary" style="border:solid 6px #000;">
            <!-- <ul> -->
                <!-- <li style="list-style:none;"><a href="" class="btn btn-warning" style="width:150px;height:50px;">+Cientes</a></li> -->
                <!-- <li style="list-style:none;"><a href="" class="btn btn-warning" style="width:150px;height:50px;">+Productos</a></li> -->
                <!-- <li style="list-style:none;"><a href="" class="btn btn-warning" style="width:150px;height:50px;">+Cotizaciones</a></li> -->
                <!-- <li style="list-style:none;"><a href="" class="btn btn-warning" style="width:150px;height:50px;">+Administración</a></li> -->
                <!-- <li style="list-style:none;"><a href="" class="btn btn-warning" style="width:150px;height:50px;">&&</a></li> -->
                <!-- <li style="list-style:none;"><a href="" class="btn btn-warning" style="width:150px;height:50px;">%%</a></li> -->
            <!-- </ul> -->
<div style="padding:10px;"><a href="" class="btn btn-warning text-left" style="width:160px;height:50px;font-size:18px;font-weight:bold;">+Cientes</a></div>
<div style="padding:10px;"><a href="" class="btn btn-warning text-left" style="width:160px;height:50px;font-size:18px;font-weight:bold;">+Productos</a></div>
<div style="padding:10px;"><a href="" class="btn btn-warning text-left" style="width:160px;height:50px;font-size:18px;font-weight:bold;">+Cotizaciones</a></div>
<div style="padding:10px;"><a href="" class="btn btn-warning text-left" style="width:160px;height:50px;font-size:18px;font-weight:bold;">+Administración</a></div>
<div style="padding:10px;"><a href="" class="btn btn-warning text-left" style="width:160px;height:50px;font-size:18px;font-weight:bold;">&&</a></div>
<div style="padding:10px;"><a href="" class="btn btn-warning text-left" style="width:160px;height:50px;font-size:18px;font-weight:bold;">%%</a></div>

        </div>

    <!-- Aquí va el contenido  -->
        <div class="col col-lg-10">@yield('content')</div>

    </div>

    <!-- Footer -->
    <div class="row bg-dark text-light"><h5>ESTE ES EL FOOTER</h5></div>

</div>


<script>
    // $("#tblUsuarios").dataTable();
</script>

</body>
</html>
